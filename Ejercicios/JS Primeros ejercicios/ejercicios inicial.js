/* 1 */

function numeros(a, b) {
  console.log("El primer numero es " + a + " y el segundo numero es " + b);
}

numeros(2, 3);

/* 2 */

function mayor(a, b, c) {
  if (a > b && a > c) {
    console.log("El numero mayor es " + a);
  } else if (b > a && b > c) {
    console.log("El numero mayor es " + b);
  } else {
    console.log("El numero mayor es " + c);
  }
}

mayor(37, 8, 4);

/* 3 */

function calcula(a, b) {
  let s = a + b;
  console.log(s);

  let r = a - b;
  console.log(r);

  let p = a * b;
  console.log(p);

  let d = a / b;
  console.log(d);
}

calcula(4, 5);

/* 4 */

function porcentaje(android, ios) {
  console.log("El porcentaje de android es " + android + "%.");
  console.log("El porcentaje de ios es " + ios + "%.");
}

porcentaje(40, 60);

/* 5 */

function compara(a, b) {
  if (a == b) {
    console.log("Ambos numeros son iguales");
  } else if (a > b) {
    console.log("El numero mayor es " + a);
  } else {
    console.log("El numero mayor es " + b);
  }
}

compara(17, 10);

/* 6 */

function operacion(a, b, c) {
  if (c == "s") {
    let suma = a + b;
    console.log("La suma es " + suma);
  } else if (c == "m") {
    let mult = a * b;
    console.log("La suma es " + mult);
  } else {
    console.log("Operacion desconocida");
  }
}

operacion(2, 4, "r");

/* 7 */

function anyo(num) {
  if (num % 4 == 0 && num % 100 !== 0) {
    console.log("El año " + num + " es bisiesto.");
  } else if (num % 400 == 0) {
    console.log("El año " + num + " es bisiesto.");
  } else {
    console.log("El año " + num + " no es bisiesto.");
  }
}

anyo(365);

/* 8 NO FUNCIONA LA ULTIMA FUNCION */



function calculos(lista) {
  let suma = 0;
  let masalto = 0;
  let masbajo = Math.min(...lista);
  let count = 0;
  let countPares = 0;
  let countImpares = 0;

  for (let i = 0; i < lista.length; i++) {
    let itemActual = lista[i];
    suma += itemActual;

    promedio = suma / lista.length;

    if (itemActual > masalto) {
      masalto = itemActual;
    }

    if (itemActual % 2 == 0) {
      countPares++;
    } else {

      countImpares = countImpares + 1;
    }
  }
  console.log("La suma total es " + suma);
  console.log("El promedio es: " + promedio);
  console.log("El valor más alto es " + masalto);
  console.log("El valor más bajo es " + masbajo);
  console.log("Hay " + countPares + " numeros pares.");
  console.log("Hay " + countImpares + " numeros impares.")
}

calculos([5, 1, 3, 4]);


/* 9  */
function gira(cadena) {
  return cadena.split("").reverse().join("");
}

let inverso = gira("barcelona");
console.log(gira("barcelona"));

/* 10  */

/********************************/
let animals = ["cat", "dog", "rat", "bird"];

function log(myArray) {
  for (let i = 0; i < myArray.length; i++) {
    console.log(animals[i]);
  }
}

log(animals);

/*CLASE 7/03*/

function saluda(n) {
  console.log("hola " + n);
}

saluda("sara");

/*se usa m'as en react*/
let saluda2 = function (n) {
  console.log("adios " + n);
};

saluda2("luis");

/*ARROW FUNCTION
se usan m'as para funciones de un solo uso 
y o se ponen como argumento dentro de otra */

let saluda3 = (n) => console.log("bienvenido " + n);
saluda3("juan");

function doble(a) {
  return a + a;
}

let doble = (a) => a + a; //no hace falta anadir "function" ni => "return"

//BUCLES

let lista = [1, 2, 4];

for (let elemento in lista) {
  console.log(elemento);
}

//?????????/

let lista = [1, 2, 4, 5, 3, 6, 7, 3];

//para recorrer los elementos dentro de un array
function muestra(x) {
  for (let elemento of x) {
    console.log(elemento);
  }
}

function sumaArray(l1) {
  let total = 0;
  for (let el of l1) {
    total = total + el;
  }
  console.log(el);
}

sumaArray(lista);

//???

lista = [1, 2, 3, 4, 8, 2, 5];

function sumaArray(l1) {
  //solo suma
  let total = 0;
  for (let el of l1) {
    total = total + el;
  }
  console.log("El total es: " + total);
}

sumaArray(lista);

function sumaArray(l1) {
  //suma y numero maximo
  let total = 0;
  let maximo = 0;
  for (let el of l1) {
    total = total + el;
    if (maximo < el) {
      maximo = el;
    }
  }
  console.log("El numero maximo es: " + maximo);
  console.log("El total es: " + total);
}
sumaArray(lista);

////////////////
for (let i = 0; i < 3; i++) {
  console.log("3 veces");
}
//////////BUCLE WHILE//////////////

let x = 0;
while (x < 3) {
  console.log("3 veces while");
  x++;
}

//WHILE cuando no sé cuántas veces se debe repetir en bucle.
//solo sé y le puedo indicar la condición para terminar el bucle
let alarma = false;
while (alarma == false) {
  let temperatura = consulta();
  if (temperatura > 30) {
    alarma = true;
  }
}

///////////OBJETOS/////////////////

const contacto1 = {
  nombre: "ana roca",
  email: ["ana@gmail.com", "ana@hotmail.com"],
  "telefono del contacto": "23452345",
};
//atributos: nombre, email, telefono

console.log(contacto1.email[1]);

contacto1.idioma = "catala"; // se pueden definir de forma dinamica en JS

console.log(contacto1);

///// FUNCIONES DENTRO DE OBJETOS//////

const contacto1 = {
  nombre: "ana roca",
  email: ["ana@gmail.com", "ana@hotmail.com"],
  "telefono del contacto": "23452345",
  saluda: function () {
    console.log("Hola, me llamo " + this.nombre); //se refiere al objeto en el que estoy. dentro de la funci'on nombre no esta definido, por eso se necesita this. asi se refiere al objeto mismo.
    console.log("Mi numero es: " + this["telefono del contacto"]);
  },
};
//atributos: nombre, email, telefono
contacto1.nombre = "pepa paper";
contacto1.saluda();

////MODELO MAS CARO DE UN OBJETO////

const lista_motos = [
  {
    model: "YAMAHA X-MAX 250",
    preu: 1300,
    kilometres: 68000,
    cilindrada: 249,
    lloc: "Barcelona",
    any: 2007,
  },
  {
    model: "SUZUKI BURGMAN 200",
    preu: 1200,
    kilometres: 45000,
    cilindrada: 200,
    lloc: "Barcelona",
    any: 2007,
  },
  {
    model: "HONDA FORZA 250 X",
    preu: 1599,
    kilometres: 15000,
    cilindrada: 250,
    lloc: "Barcelona",
    any: 2007,
  },
  {
    model: "HONDA SCOOPY SH150i",
    preu: 1600,
    kilometres: 49000,
    cilindrada: 152,
    lloc: "Barcelona",
    any: 2006,
  },
  {
    model: "HONDA SCOOPY SH150i",
    preu: 1400,
    kilometres: 54000,
    cilindrada: 152,
    lloc: "Barcelona",
    any: 2006,
  },
  {
    model: "HONDA SCOOPY SH150i",
    preu: 1000,
    kilometres: 40200,
    cilindrada: 152,
    lloc: "Barcelona",
    any: 2004,
  },
  {
    model: "HONDA FORZA 250 X",
    preu: 1200,
    kilometres: 69000,
    cilindrada: 249,
    lloc: "Barcelona",
    any: 2006,
  },
  {
    model: "KYMCO Super Dink 300i",
    preu: 1500,
    kilometres: 64500,
    cilindrada: 300,
    lloc: "Barcelona",
    any: 2010,
  },
];

/*for(moto of lista_motos) {
console.log(moto.model);
}*/

precioMaximo = 0;
masCara = null;

for (let moto of lista_motos) {
  if (moto.preu > precioMaximo) {
    precioMaximo = moto.preu;
    masCara = moto;
  }
}
console.log(masCara.model);

/*11*/

let listaMotos = [
  {
    model: "YAMAHA X-MAX 250",
    preu: 1300,
    kilometres: 68000,
    cilindrada: 249,
    lloc: "Barcelona",
    any: 2007,
  },
  {
    model: "SUZUKI BURGMAN 200",
    preu: 1200,
    kilometres: 45000,
    cilindrada: 200,
    lloc: "Barcelona",
    any: 2007,
  },
  {
    model: "HONDA FORZA 250 X",
    preu: 1599,
    kilometres: 15000,
    cilindrada: 250,
    lloc: "Barcelona",
    any: 2007,
  },
  {
    model: "HONDA SCOOPY SH150i",
    preu: 1600,
    kilometres: 49000,
    cilindrada: 152,
    lloc: "Barcelona",
    any: 2006,
  },
  {
    model: "HONDA SCOOPY SH150i",
    preu: 1400,
    kilometres: 54000,
    cilindrada: 152,
    lloc: "Barcelona",
    any: 2006,
  },
  {
    model: "HONDA SCOOPY SH150i",
    preu: 1000,
    kilometres: 40200,
    cilindrada: 152,
    lloc: "Barcelona",
    any: 2004,
  },
  {
    model: "HONDA FORZA 250 X",
    preu: 1200,
    kilometres: 69000,
    cilindrada: 249,
    lloc: "Barcelona",
    any: 2006,
  },
  {
    model: "KYMCO Super Dink 300i",
    preu: 1500,
    kilometres: 64500,
    cilindrada: 300,
    lloc: "Barcelona",
    any: 2010,
  }
];

// const motos = (lista, buscar) => {
//  count=0;
//  lista.forEach(el => {

//    if(el.model.includes(buscar)) {
//      console.log(el.model);
//      count++;
//    }

//  })
//   console.log("Hay "+count+" modelos de la marca "+buscar);
// };



// motos(listaMotos, 'HONDA')



function motos(lista, buscar) {
  count = 0;
  lista.forEach(elementoObjeto => {
    if (elementoObjeto.model.includes(buscar)) {
      console.log(elementoObjeto.model);
      count++;
    }

  })
  console.log("Hay " + count + " modelos de la marca " + buscar);
};


motos(listaMotos, 'HONDA');

/*12*/


function listaPrecio(lista, precioMin, precioMax) {
  count = 0;
  count++;
  lista.forEach(elementoObjeto => {
    if (elementoObjeto.preu < precioMax && elementoObjeto.preu > precioMin) {
      console.log(elementoObjeto.model":"elementoObjeto.preu);
    }

  });
  console.log("Hay " + count + " modelos entre los precios " + precioMin + " y " + precioMax);
};


listaPrecio(listaMotos, 100, 2000);

/*13*/

//CON FOREACH
function listaPrecio(lista, precioMin, precioMax) {
  count = 0;

  lista.forEach(elementoObjeto => {
    if (elementoObjeto.preu < precioMax && elementoObjeto.preu > precioMin) {
      console.log(`${elementoObjeto.model}:${elementoObjeto.preu}`);
    }
    count++;
  });
  console.log("Hay " + count + " modelos entre los precios " + precioMin + " y " + precioMax);

};


listaPrecio(listaMotos, 100, 2000);



// CON FOR OF 
function listaPrecio(lista, precioMin, precioMax) {
  let count = 0;
  for (let elemento of lista) {
    if (elemento.preu < precioMax && elemento.preu > precioMin) {
      console.log(`Hola ${elemento.model}:${elemento.preu}`);
      count++;
    }

  };
  console.log("Hay " + count + " modelos entre los precios " + precioMin + " y " + precioMax);
}



listaPrecio(listaMotos, 1200, 1500);



///////////////////////////////////////////

let precioMin = lista.preu[0];


if (elemento.preu < precioMin) {
  precioMin = elemento.preu;
  console.log(precioMin);

  ///primera parte bien  
  function analiza(lista) {
    let precioMax = 0;
    let masCaro = null;


    for (let elemento of lista) {

      if (elemento.preu > precioMax) {
        precioMax = elemento.preu;
        masCaro = elemento.model;
      }
    }

    console.log("La moto mas cara es " + masCaro + " y cuesta " + precioMax + " euros.");

  }

  analiza(listaMotos);


  /*Recibe una lista (array) de motos y devuelve los siguientes datos:
  moto más cara (modelo y precio)
  moto más económica (modelo y precio)
  moto con mas km (modelo, precio y km)
  marca más repetida (marca, núm de motos)
  */


  function analiza(lista) {

    //VARIABLES PARA USAR EN EL BUCLE
    let precioMax = 0;
    let masCaro = null;
    let precioMin = lista[0].preu;
    let masBarato = null;
    let km = 0;
    let masKm = 0;
    let precioMasKm = null;


    //ABRE BUCLE
    for (let elemento of lista) {

      if (elemento.preu > precioMax) {  //mas cara
        precioMax = elemento.preu;
        masCaro = elemento.model;
      }

      if (elemento.preu < precioMin) { //mas barata
        precioMin = elemento.preu;
        masBarato = elemento.model;
      }


      if (elemento.kilometres > km) {  //mas kilometros
        km = elemento.kilometres;
        masKm = elemento.model;
        precioMasKm = elemento.preu;
      }

      function extraerPalabra(elemento) {
        return elemento.model.split(' ')[0];
      }
      let marcas = extraerPalabra(elemento);
      console.log(marcas);///////


    }  //CIERRA BUCLE

    //ACCIONES
    console.log(`La moto más barata es ${masBarato} y cuesta ${precioMin} euros.`);
    console.log(`La moto más cara es ${masCaro} y cuesta ${precioMax} euros.`);
    console.log(`La moto con más km es ${masKm}, cuesta ${precioMasKm} y tiene ${km} kms.`);
    //console.log((`La marca más repetida es ${masRepetida}. Hay ${numeroRepetidas} motos de esta marca.`);
  }



  analiza(listaMotos);


  /*let marca = lista[0].model.split(' ')[0]; //extraer primera palabra de la cadena
  console.log(marca);*/




  let numero = 0;

  while (numero >= 1 && numero <= 10) {
    console.log(`Numero incorrecto, entrar de nuevo.`)
  }
  let numeroCuadrado = Math.pow(numero, 2);
  console.log(numero);



  ///OPCION1

  function encontrarCuadrado(numero) {

    function numeroEnRango(numero, min, max) {
      return numero >= min && numero <= max;
    }
    let enRango = numeroEnRango(numero, 1, 10);
    console.log(enRango);


    while (enRango == false) {
      let numeroCuadrado = Math.pow(numero, 2);
      console.log(numeroCuadrado);
    }


    console.log(`Numero incorrecto, entrar de nuevo.`);
  }


  encontrarCuadrado(5);

  ////BUCLES
  ///OPCION 2////FUNCIONA!!! (PERO REHACER EN BUCLE)

  function encontrarCuadrado(numero) {

    let numeroEnRango = enRango(numero, 1, 10);

    function enRango(numero, min, max) {
      return numero >= min && numero <= max;
    }
    //console.log(numeroEnRango);


    if (numeroEnRango == true) {
      let numeroCuadrado = Math.pow(numero, 2);
      //  console.log(numeroCuadrado);
      prompt(`El cuadrado de ${numero} es ${numeroCuadrado}.`);
    } else {
      prompt(`Numero incorrecto. Introducir de nuevo.`)
    }

  }// cierra funcion encontrarCuadrado


  encontrarCuadrado(5);
  encontrarCuadrado(3);
  encontrarCuadrado(0);
  encontrarCuadrado(12);
  encontrarCuadrado(10);



  //FUNCION 3 CON DO Y WHILE... FUNCIONA

  function encontrarCuadrado(numero) {
    let num;
      do{
        num = prompt("entra numero");
        if (num>0 && num<11){
          alert("el cuadrado es " + (num*num))
        } else {
          alert("numero no valido " )
        }
        
      } while (num>0)
////////////////


  let suma = num => {

    let total = 0;

    if (num <= 0) {
      console.log(`La suma de los valores introducidos es ${total}`);
      console.log(total);
    }
    total += num;
    console.log(${ num }, total: ${ total });
  }

  suma(-8);

  let total = 0;




function suma(num) {

    if (num <= 0) {
      prompt(`La suma de los valores introducidos es ${total}`);
      return;
    }
  
    total += num;  
    prompt(`Introduzca un número`);
    return suma(num);
    
  }

  suma(-8);

  //////////////////////////////USO DE MAPS///////////

  let numeros = [3, 6,7,4]; //el array con elementos que quiero transformar

  function mult(num){     //creo la funcion que multiplica y nombre inventado del elemento que voy a usar
    return num*num;
  }
  
  
  let multiplicacion=numeros.map(mult);  //aplico la funcion mult a cada elemento del array numeros.
  
  console.log(multiplicacion);

  ///FORMA ARROW
  
let numeros = [3, 6,7,4];

let multiplicacion=numeros.map(num=>{return num*num});

console.log(multiplicacion);

///////METODO MAPS. SUBIR DE PRECIO A CADA ITEM PREU DEL ARRAY_MOTOS/////

const array_motos = [
  {
      "model": "YAMAHA X-MAX 250",
      "preu": 1300,
      "kilometres": 68000,
      "cilindrada": 249,
      "lloc": "Barcelona",
      "any": 2007
  },
  {
      "model": "SUZUKI BURGMAN 200",
      "preu": 1200,
      "kilometres": 45000,
      "cilindrada": 200,
      "lloc": "Barcelona",
      "any": 2007
  },
  {
      "model": "HONDA FORZA 250 X",
      "preu": 1599,
      "kilometres": 15000,
      "cilindrada": 250,
      "lloc": "Barcelona",
      "any": 2007
  },
  {
      "model": "HONDA SCOOPY SH150i",
      "preu": 1600,
      "kilometres": 49000,
      "cilindrada": 152,
      "lloc": "Barcelona",
      "any": 2006
  },
  {
      "model": "HONDA SCOOPY SH150i",
      "preu": 1400,
      "kilometres": 54000,
      "cilindrada": 152,
      "lloc": "Barcelona",
      "any": 2006
  }
];

//en la variable subidaPrecio guardo el nuevo array con precios actualizados
let subidaPrecio=array_motos.map(item=>{ //item sería cada elemento(que contiene la propiedad y el valor)
let precioNuevo= item.preu+50;
return precioNuevo;
})


console.log(subidaPrecio);

//////FACTORIAL/////


function factorial(num){
  if(num<=0){
    return 1;
  } else{
    return factorial(num-1)*num;
  }
}

let numero=5;
console.log(`El factorial de ${numero} es ${factorial(numero)}`);

////////////////////////////////////// M A P ///////////////////////////////////////////////////////

////CON MAP MULTIPLICAR POR EL NUMERO INDICADO. 
let lista=[2, 5, 100];

function multiplyBy(arr,num){
 let nuevoArray = arr.map(item => item *=num); ///ES NECESARIO GUARDAR EL ARR.MAP EN UNA VARIABLE DECLARADA
  return nuevoArray;
}


console.log(multiplyBy(lista,6));
/////////// DUPLICAR CON MAP

function doubleNumbers(arr){
  let nuevoArray = arr.map(item =>item*=2);
  return nuevoArray;
}

console.log(doubleNumbers([2, 5, 100])); 

